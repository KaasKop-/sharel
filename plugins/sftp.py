import importlib
from config_handler import ConfigHandler

class sftp:
    service_name = "sftp"
    help_text = "Uploads the files to an SFTP server"
    nargs = 1
    setup_succeeded = False

    def __init__(self):
        try:
            self.paramiko = importlib.import_module("paramiko")
        except ImportError as e:
            print(e.with_traceback(None))
        self.config_handler = ConfigHandler(self.service_name)
        self.config_handler.create_new_section_if_not_exists(
            {
                "domain": "sftp-is-not-configured", 
                "username": "root(plsdont)", 
                "password": "SuperSecretPassword", 
                "port": "22", 
                "use_pub_key_authentication": True, 
                "remote_dir": "/var/www/example.com/files", 
                "http_path": "http://example.com/files/", 
                "notification_clipboard_content": "http_path"
            })
        self.sftp_section = self.config_handler.get_section()
        self.domain = self.sftp_section["domain"]
        self.port = int(self.sftp_section["port"])
        self.username = self.sftp_section["username"]
        self.password = self.sftp_section["password"]
        self.remote_dir = self.sftp_section["remote_dir"]
        self.use_pub_key = bool(self.sftp_section["use_pub_key_authentication"])
        self.http_path = self.sftp_section["http_path"]
        self.notification_content = self.sftp_section["notification_clipboard_content"]
        self.setup_succeeded = True


    def upload(self, file):
        print("Uploading to SFTP")

        return [True, "", ]
