from helpers.web_api_helper import WebApiHelper
from config_handler import ConfigHandler
from pathlib import Path
import json

class imgur:
    service_name = "imgur"
    help_text = "Upload the file to imgur."
    nargs = 1
    setup_succeeded = True

    def __init__(self):
        self.config_handler = ConfigHandler(self.service_name)
        self.web_api_helper = WebApiHelper("https://api.imgur.com/3/")
        self.config_handler.create_new_section_if_not_exists({"client-id": "b0b233b63c847b4", "username": "optional", "password": "optional"})
        self.imgur_configuration = self.config_handler.get_section()

    def upload(self, file):
        file = Path(file)
        # Imgur allows files up to 10.x mb its not dead set on 10mb.
        if not file.resolve().stat().st_size >= 11000000:
            data = {'image': file.read_bytes()}
            header = {"Authorization": "Client-ID " + self.imgur_configuration["client-id"]}

            req = self.web_api_helper.post("image", data, header)
            
            if not req:
                return [False, "", req]
            return [True, json.loads(req.content)["data"]["link"], req]
        else:
            return [False, "File exceeds imgur's maximum upload size of <10.9mb."]

