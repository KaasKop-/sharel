import pkgutil
import importlib
import argparse
from pathlib import Path
from config_handler import ConfigHandler

sharel_plugins = {}
conf_handler = ConfigHandler("general")
result = None

for x in Path("./plugins").iterdir():
    if x.is_file():
        sharel_plugins[x.parts[-1][:-3]] = getattr(importlib.import_module("plugins." + x.parts[-1][:-3]), x.parts[-1][:-3])

parser = argparse.ArgumentParser(prog="ShareL", description="Upload files to remote hosts")
parser.add_argument("--edit-conf", help="Edit the configuration of the program (uses the $EDITOR variable)", dest="ec")
parser.add_argument("--cli", help="This forces ShareL to run in CLI mode", dest="cli")

# Here we load plugins and add them to argparse
for key, value in sharel_plugins.items():
    parser.add_argument("--" + value.service_name, help=value.help_text, dest=value.service_name, nargs=value.nargs)

args = parser.parse_args()

if args.ec:
    print("If called we'll start a session with their $EDITOR and edit the default location of the configuration")
elif args.cli:
    print("Starting ShareL in CLI mode...")

# Here we check if a plugin is called and if so we make an object and execute the upload
for key, value in vars(args).items():
    if key in sharel_plugins and value is not None:
        plugin = sharel_plugins[key]()
        if plugin.setup_succeeded:
            result = plugin.upload(value[0]) #TODO Allow for more than one parameter
            if result[0]:
                print("File uploaded: " + result[1])
                conf_handler.apply_general_config_options(result[1])
            elif not result[0]:
                print("Something went wrong")
                #print("Something went wrong." + result[2].status_code)
        

#TODO Here we handle all the general configuration options and apply them as needed.


