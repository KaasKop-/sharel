import configparser
import importlib
from pathlib import Path
from helpers.misc_helper import MiscHelper

class ConfigHandler:
    def __init__(self, service):
        self.service = service
        self.conf_parser = configparser.ConfigParser()
        self.config_path = Path("~/.config/ShareL")
        self.config_file = Path("~/.config/ShareL/config.ini")

        if not self.config_path.expanduser().is_dir() :
            self.config_path.expanduser().mkdir()
        if not self.config_file.expanduser().is_file():
            print("Making the config file.")
            self.config_file.expanduser().write_text("")
            print("There was no configuration found, I made one in " + str(self.config_path))
            self.init_config()
        
        self.read_file = self.conf_parser.read(self.config_file.expanduser())

    def init_config(self):
        self.conf_parser["general"] = {
            "copy_link_to_clipboard": True,
            "show_notification_on_upload_initiation": True,
            "show_notification_on_upload_completed": True,
            "always_save_image_to_local_disk": True,
            "local_save_directory": Path("~/Pictures/").expanduser()
        }

        with open(self.config_file.expanduser(), "w") as f:
            self.conf_parser.write(f)

    def does_section_exist(self):
        if self.conf_parser.has_section(self.service):
            return True
        return False

    def does_key_exist_in_section(self, key):
        if self.does_section_exist() and key in self.conf_parser[self.service].keys():
            return self.conf_parser[self.service][key]
        return False

    def get_section(self):
        if self.does_section_exist():
            return self.conf_parser[self.service]
        return False

    def get_key_value(self, key):
        if self.does_section_exist() and key in self.conf_parser[self.service].keys():
            return self.conf_parser[self.service][key]
        elif not self.does_section_exist():
            print("Section " + self.service + " does not exist.")
        elif not self.does_key_exist_in_section(key):
            print("Key " + key + " does not exist in section " + self.service)
        return False

    def create_new_section_if_not_exists(self, values):
        if self.does_section_exist():
            return False
        self.conf_parser[self.service] = values

        with open(self.config_file.expanduser(), "w") as f:
            self.conf_parser.write(f)
            return True
        return False

    def set_key_value(self, key, value):
        # you'll be able to set settings in the config file in the future.
        pass

    # The key show_notification_on_upload setting is handled in main.py
    def apply_general_config_options(self, upload_details):
        if bool(self.get_key_value("copy_link_to_clipboard")):
            MiscHelper.copy_to_clipboard("MEMES!")
        # if bool(self.get_key_value("show_notification_on_upload_initiation")):
        #     self.misc.send_notification("Started upload.")

        if bool(self.get_key_value("show_notification_on_upload_completed")):   
            #TODO Custom message?         
            MiscHelper.send_notification("The file was uploaded successfully.")
        
