import importlib

class MiscHelper:
    @staticmethod
    def copy_to_clipboard(data):
        if data:
            try:
                pyperclip = importlib.import_module("pyperclip")
                pyperclip.copy(data)
            except ImportError as e:
                print(e.with_traceback(None))

    @staticmethod
    def send_notification(text):
        try:
            notify2 = importlib.import_module("notify2")
            notify2.init("ShareL")
            notify2.Notification(text).show()
        except ImportError as e:
            print(e.with_traceback(None))

    @staticmethod
    def send_stdout(text):
        print(text)

    @staticmethod
    def get_filename(file_location):
        return file_location.split("/")[-1]
